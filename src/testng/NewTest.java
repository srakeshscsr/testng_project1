package testng;

import org.testng.annotations.Test;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;

public class NewTest {
  @Test(priority=1)
  public void f() {
	  System.out.println("test1");
  }
  @AfterClass
  public void afterClass() {
	  System.out.println("test2");
  }

  @BeforeTest
  public void beforeTest() {
	  System.out.println("test3");
  }

}
